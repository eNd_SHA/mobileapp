import React, {Component} from 'react';
import{Text, View, StyleSheet} from 'react-native';
import Video from 'react-native-video';

export default class StreamScreen extends Component{

  constructor(props)
  {
    super(props)
  }

  render() {
    return (
      <View style={styles.viewContainer}>
        <Video source={{uri: this.props.navigation.state.params.link}}   // Can be a URL or a local file.
          ref={(ref) => {
            this.player = ref
          }}                                      // Store reference
          onBuffer={this.onBuffer}                // Callback when remote video is buffering
          onError={this.videoError}               // Callback when video cannot be loaded
          style={styles.backgroundVideo} 
          resizeMode={"contain"}
          controls={true}
          playInBackground={this.props.navigation.state.params.background}
        />
      </View>
    );
  }
}

var styles = StyleSheet.create({
  backgroundVideo: {
    position: 'absolute',
    top: 0,
    left: 0,
    bottom: 0,
    right: 0,
  },
  viewContainer: {
    flex: 1,
    backgroundColor: 'black',
  }
});