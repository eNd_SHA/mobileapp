import React, {Component} from 'react';
import {View, Text} from 'react-native';
import { FlatList, TouchableOpacity } from 'react-native-gesture-handler';
import { Card, Image } from 'react-native-elements';

export default class Home extends Component{

    constructor(props)
    {
        super(props);
        this.state = {
            data: [
                {
                    name: "VTV1",
                    background: false,
                    img: "https://content.vtvgiaitri.vn/channel_logo/W_V1_Eb8vhXa.png",
                    uri: "https://hjyjrvmlsk.vcdn.com.vn/hls/elgfjdh/index.m3u8",
                },
                {
                    name: "VTV2",
                    background: false,
                    img: "https://content.vtvgiaitri.vn/channel_logo/W_V2.png",
                    uri: "https://hjyjrvmlsk.vcdn.com.vn/hls/m0hlc1s/index.m3u8",
                },
                {
                    name: "VTV3",
                    background: false,
                    img: "https://content.vtvgiaitri.vn/channel_logo/W_V3.png",
                    uri: "https://hjyjrvmlsk.vcdn.com.vn/hls/idmwifi/index.m3u8",
                },
                {
                    name: "VTV4",
                    background: false,
                    img: "https://content.vtvgiaitri.vn/channel_logo/W_V4.png",
                    uri: "https://hjyjrvmlsk.vcdn.com.vn/hls/hktebdo/index.m3u8",
                },
                {
                    name: "VTV5",
                    background: false,
                    img: "https://content.vtvgiaitri.vn/channel_logo/W_V5.png",
                    uri: "https://kcdn-livesport.vtvgiaitri.vn/vtvlivesport/vtv5_720.m3u8",
                },
                {
                    name: "VTV6",
                    background: false,
                    img: "https://content.vtvgiaitri.vn/channel_logo/W_V6.png",
                    uri: "https://hjyjrvmlsk.vcdn.com.vn/hls/fjg6388/index.m3u8",
                },
                {
                    name: "VTV7",
                    background: false,
                    img: "https://content.vtvgiaitri.vn/channel_logo/W_V7_10HKiVA.png",
                    uri: "https://hjyjrvmlsk.vcdn.com.vn/hls/nwalmjv/index.m3u8",
                },
                {
                    name: "VTV8",
                    background: false,
                    img: "https://content.vtvgiaitri.vn/channel_logo/W_V8.png",
                    uri: "https://hjyjrvmlsk.vcdn.com.vn/hls/wgabpze/index.m3u8",
                },
                {
                    name: "VTV9",
                    background: false,
                    img: "https://content.vtvgiaitri.vn/channel_logo/W_V9_LTfTUAq.png",
                    uri: "https://hjyjrvmlsk.vcdn.com.vn/hls/ig9jpqw/index.m3u8",
                },
                {
                    name: "Youtube Music",
                    background: true,
                    img: "https://www.aero-mag.com/wp-content/uploads/2017/10/unnamed-1.png",
                    uri: "https://manifest.googlevideo.com/api/manifest/hls_playlist/id/mmRIVSrJbbc.381/itag/95/source/yt_live_broadcast/requiressl/yes/ratebypass/yes/live/1/cmbypass/yes/goi/160/sgoap/gir%3Dyes%3Bitag%3D140/sgovp/gir%3Dyes%3Bitag%3D136/hls_chunk_host/r4---sn-8pxuuxa-nboez.googlevideo.com/ei/vViJXNr9MZSGz7sPktes2AU/gcr/vn/playlist_type/DVR/initcwndbps/6900/mm/32/mn/sn-8pxuuxa-nboez/ms/lv/mv/m/pcm2cms/yes/pl/21/dover/11/keepalive/yes/mt/1552504952/disable_polymer/true/ip/116.109.153.194/ipbits/0/expire/1552526621/sparams/ip,ipbits,expire,id,itag,source,requiressl,ratebypass,live,cmbypass,goi,sgoap,sgovp,hls_chunk_host,ei,gcr,playlist_type,initcwndbps,mm,mn,ms,mv,pcm2cms,pl/signature/6B077EF2310320BE4D780DFE128A28BD4BB22CD0.64BFD6DFA3087D68E946E7D464861E629EB08EBF/key/dg_yt0/playlist/index.m3u8"
                },
                {
                    name: "freeCodeCamp Channel",
                    background: true,
                    img: "https://yt3.ggpht.com/a-/AAuE7mDMgJdxLr67xIch3lj0egc9RZXiZhMXIglFew=s900-mo-c-c0xffffffff-rj-k-no",
                    uri: "https://manifest.googlevideo.com/api/manifest/hls_playlist/id/mXRfApkMYZU.0/itag/95/source/yt_live_broadcast/requiressl/yes/ratebypass/yes/live/1/cmbypass/yes/goi/160/sgoap/gir%3Dyes%3Bitag%3D140/sgovp/gir%3Dyes%3Bitag%3D136/hls_chunk_host/r1---sn-8pxuuxa-nboes.googlevideo.com/gcr/vn/playlist_type/DVR/ei/YFqJXMjrN8On3LUP8PGi4A0/initcwndbps/9400/mm/32/mn/sn-8pxuuxa-nboes/ms/lv/mv/m/pcm2cms/yes/pl/21/dover/11/keepalive/yes/manifest_duration/30/playlist_duration/30/mt/1552505337/disable_polymer/true/ip/116.109.153.194/ipbits/0/expire/1552527041/sparams/ip,ipbits,expire,id,itag,source,requiressl,ratebypass,live,cmbypass,goi,sgoap,sgovp,hls_chunk_host,gcr,playlist_type,ei,initcwndbps,mm,mn,ms,mv,pcm2cms,pl/signature/24DF21FF7D5B5D6311E74AA74F8A292F914C8C2E.5A33342AB149CBF73FD562446AC5B1A4AFC4F046/key/dg_yt0/playlist/index.m3u8"
                }
            ]
        }
    }

    renderItem = ({item}) => {
        return(
            <TouchableOpacity onPress={() => {this.props.navigation.navigate("Stream", {link: item.uri, background: item.background})}}>
                <Card wrapperStyle={{
                    flexDirection: 'row',
                    alignItems: 'center'
                }}>
                    <Image
                        source={{uri: item.img}}
                        style={{width: 120, height: 100, marginRight: 30}}
                    />
                    <Text
                        style={{fontSize: 15}}
                    >{item.name}</Text>
                </Card>
            </TouchableOpacity>
        )
    }

    render(){
        return(
            <View style={{
                flex: 1,
                backgroundColor: '#f8f8f9',
            }}>
                <FlatList
                    data={this.state.data}
                    renderItem={this.renderItem}
                    ListFooterComponent={<View style={{ marginBottom: 20, }} />}
                />
            </View>
        )
    }
}