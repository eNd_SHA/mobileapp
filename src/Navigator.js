import {createStackNavigator, createAppContainer} from 'react-navigation'
import Home from './Home'
import StreamScreen from './StreamScreen'

const Stack = createStackNavigator(
    {
        Home: {
            screen: Home,
            navigationOptions: ({navigation})=>({
                title: "TV STREAM",
                headerStyle:{
                    backgroundColor: "#003300",
                },
                headerTitleStyle:{
                    color: '#f8f8f9',
                    flex: 1,
                    textAlign: 'center',
                }
            })
        },
        Stream:{
            screen: StreamScreen,
            navigationOptions: ({navigation})=>({
                header: null,
            })
        }
    },
    {
        initialRouteName: 'Home'
    }
)

const AppContainer = createAppContainer(Stack);

export default AppContainer;